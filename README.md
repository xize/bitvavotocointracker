# BitvavoToCointracker

![image](/uploads/c788a75b9e1bad8e9926c1db5ab52561/image.png)

We made this tool because we noticed alot of issues regarding support for Bitvavo for cointracking.io.

Since I aswell need to check my taxes often, I decided to write a program which automaticaly converts
the bitvavo csv transaction file to the supportive cointracker.io format.

While the developer had promised to support bitvavo, I can use this program in the mean time and it might be usefull for others
aswell.

Note:

not everything is supported, trades only show the profit income since bitvavo already calculates it themselfs.

please go releases [here](https://gitlab.com/xize/bitvavotocointracker/-/tags)
