﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BitvavoToCointracker
{
    public class Parser
    {

         /*
         * 0 = "timestamp"
         * 1 = "type"
         * 2 = "currency"
         * 3 = "amount"
         * 4 = "status"
         * 5 = "address"
         * 6 = "method"
         * 7 = "txid"
         */
        protected string bitvavo_sig = "\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\"";

        /*
         * 0 = Date
         * 1 = Received Quantity
         * 2 = Received Currency
         * 3 = Sent Quantity
         * 4 = Sent Currency,
         * 5 = Fee Amount
         * 6 = Fee Currency,
         * 7 = Tag
         */
        protected string cointracker_sig = "{0},{1},{2},{3},{4},{5},{6}";

        private string bitvavofile;
        private ProgressBar bar;
        private Label progresslabel;

        public Parser(string file, ProgressBar bar, Label progresslabel)
        {
            this.bitvavofile = file;
            this.bar = bar;
            this.progresslabel = progresslabel;
        }

        public string Parse()
        {
            //first create an stringbuilder to append.
            StringBuilder builder = new StringBuilder();

            builder.Append("Date,Received Quantity,Received Currency,Sent Quantity,Sent Currency,Fee Amount,Fee Currency,Tag");
            builder.AppendLine();

            //read the file in lines and strip illegal characters.
            string[] bitvavolines = File.ReadAllLines(this.bitvavofile);

            int count = 0;

            for (int i = 1; i < bitvavolines.Length;i++)
            {
                bar.Invoke(new MethodInvoker(delegate ()
                {
                    count++;
                    bar.Value = count/i*30+70;
                    progresslabel.Text = bar.Value+"%";
                    bar.Update();
                    progresslabel.Update();
                }));

                string line = bitvavolines[i];
                string newline = line.Replace("\"", String.Empty);

                string[] bitvavo_arguments = newline.Split(',');

                //first parse a legitimate date.
                //Example: Thu Aug 26 2021 12:55:54 GMT+0000 (Coordinated Universal Time)

                int findcurl = bitvavo_arguments[0].IndexOf('(');
                bitvavo_arguments[0] = bitvavo_arguments[0].Substring(0, (findcurl - 1)- " GMT+0000".Length).ToLower(); //remove invalid curls.

                DateTime date = DateTime.Parse(bitvavo_arguments[0].ToLower(), CultureInfo.InvariantCulture);

                //Example for cointracker: 06/14/2017 20:57:35
                string cointrackerdate = date.ToString(@"MM/dd/yyyy HH:mm:ss");
                int patchdatesize = "MM/dd/yyyy".Length;
                
                string patchedtime = cointrackerdate.Substring(0, patchdatesize).Replace("-", "/");
                string removedtime = cointrackerdate.Remove(0, patchdatesize);
                cointrackerdate = patchedtime + removedtime;


                //swap things around.
                string[] cointracker_arguments = new string[8];
                cointracker_arguments[0] = cointrackerdate; //date which we have formated.

                //determine what the 'transaction type' is in bitvavo so we can store more information into the array.
                cointracker_arguments = SwapArguments(cointracker_arguments, bitvavo_arguments);

                foreach(string data in cointracker_arguments)
                {
                    builder.Append(data+",");
                }
                builder.Remove(builder.Length - 1, 1);
                builder.AppendLine();
            }
            return builder.ToString();

        }

        private string[] SwapArguments(string[] cointracker_arguments, string[] bitvavo_arguments)
        {
            //for index 7 cointracker expects one of these:
            //'gift', 'lost', 'donation', 'mined', 'airdrop', 'payment', 'fork'

            switch (bitvavo_arguments[1])
            {
                case "withdrawal":
                    cointracker_arguments[1] = ""; //received quantity
                    cointracker_arguments[2] = ""; //received currency
                    cointracker_arguments[3] = bitvavo_arguments[3]; //sent quantity
                    cointracker_arguments[4] = bitvavo_arguments[2];
                    cointracker_arguments[5] = ""; //not supported fee is automatic calculated
                    cointracker_arguments[6] = ""; //fee is automatic calculated thus not showing symbol
                    cointracker_arguments[7] = "payment";
                    break;
                case "deposit":
                    cointracker_arguments[1] = bitvavo_arguments[3]; //quantity received
                    cointracker_arguments[2] = bitvavo_arguments[2]; //crypto type received
                    cointracker_arguments[3] = ""; //we do not sent a quantity
                    cointracker_arguments[4] = ""; //we do not sent a quantity currency
                    cointracker_arguments[5] = ""; //fees are unsupported
                    cointracker_arguments[6] = ""; //fees are unsported
                    cointracker_arguments[7] = "gift";
                    break;
                case "trade":
                    cointracker_arguments[1] = bitvavo_arguments[3]; //received quantity
                    cointracker_arguments[2] = bitvavo_arguments[2]; //received crypto
                    cointracker_arguments[3] = "";
                    cointracker_arguments[4] = "";
                    cointracker_arguments[5] = "";
                    cointracker_arguments[6] = "";
                    cointracker_arguments[7] = "payment"; //note we are limited... cointrackers API only has one amount field.
                    break;
                case "staking":
                    cointracker_arguments[1] = bitvavo_arguments[3]; //received quantity
                    cointracker_arguments[2] = bitvavo_arguments[2]; //received crypto type
                    cointracker_arguments[3] = ""; //not supported since bitvavo specifies only one amount.
                    cointracker_arguments[4] = ""; //not supported since bitvavo specifies only one amount.
                    cointracker_arguments[5] = ""; //fees not supported in bitvavo
                    cointracker_arguments[6] = ""; //fees not supported in bitvavo
                    cointracker_arguments[7] = "mined";
                    break;
                default:
                    cointracker_arguments[1] = ""; //received quantity
                    cointracker_arguments[2] = ""; //received currency
                    cointracker_arguments[3] = bitvavo_arguments[3]; //sent quantity
                    cointracker_arguments[4] = bitvavo_arguments[2];
                    cointracker_arguments[5] = ""; //not supported fee is automatic calculated
                    cointracker_arguments[6] = ""; //fee is automatic calculated thus not showing symbol.
                    cointracker_arguments[7] = "payment";
                    break;
            }
            return cointracker_arguments;
        }

    }
}
