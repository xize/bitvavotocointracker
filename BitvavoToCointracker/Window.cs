﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BitvavoToCointracker
{
    public partial class Window : Form
    {
        public Window()
        {
            InitializeComponent();
        }

        private string bitvavopath;
        public string BitvavoPath
        {
            get
            {
                return this.bitvavopath;
            }
            private set
            {
                this.bitvavopath = value;
                this.bitvavotextbox.Text = value;
                this.bitvavotextbox.Update();
            }
        }

        private string cointrackerpath;
        public string CoinTrackerPath
        {
            get
            {
                return this.cointrackerpath;
            }
            private set
            {
                this.cointrackerpath = value;
                this.cointrackertextbox.Text = value;
                this.cointrackertextbox.Update();
            }
        }

        private void openbtn_Click(object sender, EventArgs e)
        {
            this.openFileDialog1.ShowDialog();
        }

        private void savebtn_Click(object sender, EventArgs e)
        {
            this.saveFileDialog1.ShowDialog();
        }

        private async void convertbtn_ClickAsync(object sender, EventArgs e)
        {
            this.progressbar.Value = 0;
            if(this.saveFileDialog1.FileName.Length == 0 || this.openFileDialog1.FileName.Length == 0)
            {
                MessageBox.Show("please make sure you specified all locations!","Could not open or save file!",MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Parser parse = new Parser(BitvavoPath, this.progressbar, this.progresslabel);

            string result = string.Empty;

            Task<string> t = Task<string>.Run(() => parse.Parse());
            await t;

            if(t.IsCompleted)
            {
                File.WriteAllText(CoinTrackerPath, t.Result);
            }

            Process proc = Process.Start("notepad", CoinTrackerPath);

        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            this.BitvavoPath = this.openFileDialog1.FileName;
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            this.CoinTrackerPath = this.saveFileDialog1.FileName;
        }
    }
}
